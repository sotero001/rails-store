describe User do

  before(:each) { @user = User.create(name: "Dave", email: 'dave@example.com') }

  subject { @user }

  it { should respond_to(:email) }

  it "#email returns a string" do
    expect(@user.email).to match 'dave@example.com'
  end

  it "should return the correct user name" do
    expect(@user.name).to eq("Dave")      
  end

#  it "should return the correct number of users" do
#    @user = User.new(name: "Tommy", email: "tommy@gmail.com")
#    @user.save
#    expect(@user.count).to eq(1)
#  end
end

#describe User do
#  it "should save the user in the db" do
#    User.create(name: "Tommy", email: "tommy@gmail.com")
#    expect(User.count).to eq(1)
#  end
#end
