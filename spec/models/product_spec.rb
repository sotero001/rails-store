describe Product do
  it "should save a product" do
    @product = Product.create(name: "fake product", description: "fake product description", price: 50.00, image_url: "fake_product_imgage.jpg")
    expect(Product.count).to eq(1)
  end

  it "should not save a product if not all validations are met" do
    @product = Product.create(name: "fake product", description: "fake product description", price: 50.00)
    expect(Product.count).to eq(0)
  end

  it "should not save a product if the price is an invalid type" do
    @product = Product.create(name: "fake product", description: "fake product description", price: "50.00")
    expect(@product.price).not_to be(50.00)
  end

  it "returns the correct number of products in the db" do
    @product1 = Product.create(name: "product1", description: "product1 description", price: 50.00, image_url: "product1.jpg")
    @product2 = Product.create(name: "product2", description: "product2 description", price: 50.00, image_url: "product2.jpg")
    expect(Product.count).to eq(2)
  end

  it "successfully destroys a product" do
    @product = Product.create(name:"product1", description:"product1 description", price: 50.00, image_url: "product1.jpg")
    @product.destroy
    expect(Product.count).to eq(0)
  end
end
