describe ProductsController do
  describe "GET index" do
    it "sets the @products variable" do
      prod1 = Product.create(name: "prod1", description: "prod1 desc", price: 40.00, image_url: "prod1.jpg")
      prod2 = Product.create(name: "prod2", description: "prod2 desc", price: 45.00, image_url: "prod2.jpg")
      get :index
      expect(assigns(:products)).to eq([prod1,prod2])
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET new" do
    it "sets a new @product variable" do
      prod1 = Product.new
      get :new
      expect(assigns(:products)).to eq(nil)
    end

    it "renders the new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST create" do
  end

  describe "GET edit" do
  end

  describe "PATCH/PUT update" do
  end

  describe "DELETE destroy" do
  end
end
