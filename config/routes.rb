Rails.application.routes.draw do
  root to: 'visitors#index'
  devise_for :users
  resources :users
  #resources :products

  #yanked code below from nov122015 project
  #resources :orders
  #resources :line_items
  #resources :carts
  #root to: 'visitors#index'
  #devise_for :users
  #resources :users
  #resources :payments
  #add route for products/display_item
  resources :products do
    collection do
      match 'display', :via => [:get, :post]
    end
  end
  resources :line_items
  resources :carts
end
