class ProductsController < ApplicationController
  before_action :authenticate_user!, :except => [:display]
  before_action :admin_only, :except => [:display]
  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = "Successfully Added A New Product"
      redirect_to root_path
    else
      render :new
    end
  end

  def index
    @products = Product.all
  end

  def destroy
    @product = Product.find(params[:id]).destroy
    flash[:success] = "Product has been successfully destroyed"
    redirect_to root_path
  end

  def show
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(product_params)
      flash[:notice] = "Successful Product Update"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def display
    @product = Product.find(params[:content])
  end

  private
  
    def product_params
      params.require(:product).permit(:name, :description, :price, :image_url)
    end
    
    def admin_only
      unless current_user.admin?
        redirect_to :back, :alert => "Access denied."
      end
    end
end
